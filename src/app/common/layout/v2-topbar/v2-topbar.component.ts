import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, Renderer2 } from "@angular/core";
import { Session } from "../../../services/session";

@Component({
  selector: 'm-v2-topbar',
  templateUrl: 'v2-topbar.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class V2TopbarComponent implements OnInit {
  minds = window.Minds;
  theme = 'default';
  otherTheme = 'dark';

  constructor(
    protected session: Session,
    protected cd: ChangeDetectorRef,
    private renderer: Renderer2
  ) {
  }

  ngOnInit() {
    this.session.isLoggedIn(() => this.detectChanges());
    this.renderer.addClass(document.body, 'm-theme__dark');
  }

  getCurrentUser() {
    return this.session.getLoggedInUser();
  }

  detectChanges() {
    this.cd.markForCheck();
    this.cd.detectChanges();
  }

  // Toggle theme class of <body> element when bulb icon is clicked
  toggleTheme() {
    [this.theme, this.otherTheme] = [this.otherTheme, this.theme];
    //this.renderer.addClass(document.body, 'm-theme-in-transition');
    // this.renderer.removeClass(document.body, 'm-theme__' + this.otherTheme);
    // this.renderer.addClass(document.body, 'm-theme__' + this.theme);
    this.renderer.setAttribute(document.body, 'class', 'm-theme__' + this.theme);
  }
}
